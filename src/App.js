import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {Nav, NavItem, NavLink, Navbar, NavbarBrand, Table, Form, FormGroup, Label, Input, Button, Container, Alert} from 'reactstrap';


class UserForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            surname: "",
            email: "",
            message: "",
            message_type: "",
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    //Submitting a new user into the backend
    async handleSubmit(event) {
        event.preventDefault();
        this.setState({ message_type: '' }); //hiding an errro before doing anything
        const response = await fetch('http://127.0.0.1:3333/users/?key=b56051db6', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        });
        const data = await response.json();
        //console.log(data);
        if(data.hasOwnProperty('_id')) {
            //alert('User was added!');
            this.setState({
                message: 'User was added successfully!',
                message_type: 'success'
            });
            //this.props.history.push('/');
            this.setState({name: "", surname: "", email: ""});
        } else {
            this.setState({
                message: 'There was an error adding the user!',
                message_type: 'warning'
            });
            //alert('There was an error, sorry!');
        }
    }

    render() {

        return (
            <Form onSubmit={this.handleSubmit}>
                { this.state.message_type &&
                <Alert color={this.state.message_type}> {this.state.message} </Alert> }
                <FormGroup>
                    <Label for="name">Name</Label>
                    <Input type="text" value={this.state.name} id="name" name="name" required onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="surname">Surname</Label>
                    <Input type="text" value={this.state.surname} id="surname" name="surname" required onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="email">E-mail</Label>
                    <Input type="text" value={this.state.email} id="email" name="email" required onChange={this.handleChange} />
                </FormGroup>
                <Button color="primary">Submit</Button>
            </Form>
        );

    }
}

function App() {

    const [users, setUsers] = useState([]);
    const [refresh, setRefresh] = useState(0);

    function Menu() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand>Sapiens</NavbarBrand>
                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <NavLink href="/">Home</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/add">Add user</NavLink>
                        </NavItem>
                    </Nav>
                </Navbar>
            </div>
        );

    }

    //Executing call to the backend during app load
    useEffect(() => {
       getUsers();
    },[refresh]);

    //Getting list of user from the backend
    const getUsers = async () => {
        const response = await fetch("http://127.0.0.1:3333/users");
        const data = await response.json();
        setUsers(data);
        //console.log(data);
    }

    //Rendering list of user
    const UserList = () => {
        return (
            <Table>
                <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>E-mail</th>
                </tr>
                </thead>
                <tbody>
                    { users.map(user => (
                        <tr key={user._id}>
                            <td>{user.name}</td>
                            <td>{user.surname}</td>
                            <td>{user.email}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        );

    }

    //Main app rendering
    return(
        <Router>
                <Container>
                <Menu />
                    <Switch>
                        <Route path="/" exact component={UserList} />
                        <Route path="/add" component={UserForm} />
                    </Switch>
                </Container>
        </Router>
    );
}

export default App;